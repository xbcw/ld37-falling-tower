﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour {

	public static GameManager instance = null;

	public Sprite[] towerTopSprites;
	public GameObject puppyPrefab;
	public bool puppySpawned = false;

	public GameObject[] furniture;
	public Text floorName;
	public Text floorNameOutline;

	public Text message;
	public Text messageOutline;

	int sectionCount = 4;
	Color color = new Color(128,128,128);

	public string[] floorNames;
	public Floor[] floors;
	public bool reverseFloorSprites = false;

	public string[] floorNameAdjectives = new string[]{"", "North ", "South ", "East ", "West ", "Upper ", "Lower "};

	public Floor currentFloor;
	public int lastFloor = 100;
	public int floor = 1;

	public bool gameOver = false;
	public bool win = false;

	private string currentFloorName = "missingNo";
	private float sectionDamage = 0.05f;
	private float fallSpeed = 6f;

	private AudioSource source;
	public AudioClip[] sounds;

	public int lastFloorCount = 0;

	void Awake () {
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy(gameObject);
		source = GetComponent<AudioSource>();
		NewFloorName();
	}

	public void PlaySoundEffect(int idx, float delayTime) {
		StartCoroutine(PlaySound(idx, delayTime));
	}

	IEnumerator PlaySound(int idx, float delayTime) {
		yield return new WaitForSeconds(delayTime);
		source.PlayOneShot(sounds[idx]);
	}

	void PlaySound(int idx) {
		source.PlayOneShot(sounds[idx]);
	}

	void UpdateFloorname() {
		floorName.text = floorNameOutline.text = string.Format("Floor: {0} - {1}", floor, currentFloorName);
	}

	public float GetSectionDamage() {
		return sectionDamage;
	}

	public float GetFallSpeed() {
		return fallSpeed;
	}

	private void NewFloorName() {
		if (floor == 1) {
			currentFloorName = "Grand Entrance";
		} else if (currentFloor.useAdjective) {
			currentFloorName = string.Format("{0}{1}", floorNameAdjectives[Random.Range(0,floorNameAdjectives.Length)], currentFloor.roomNames[Random.Range(0,currentFloor.roomNames.Length)]);
		} else {
			currentFloorName = currentFloor.roomNames[Random.Range(0,currentFloor.roomNames.Length)];
		}
		UpdateFloorname();
	} 

	void NewFloor() {
		floor++;
		if (floor == lastFloor){
			currentFloor = new Floor();
			currentFloor.baseType = "TowerTop";
   			currentFloor.roomNames = new string[] {"Tower Top"};
    		currentFloor.sprites = new Sprite[] {towerTopSprites[0], towerTopSprites[1], towerTopSprites[2], towerTopSprites[3]};
    		currentFloor.useAdjective = false;
		} else {
			currentFloor = floors[Random.Range(0,floors.Length)];
		}
		reverseFloorSprites = Random.value > 0.5 ? true : false;
		if(floor % 20 == 0) {
			sectionDamage += 0.05f;
			SetMessage("The Tower has become more dangerous!");
			GameObject.Find("Main Camera").GetComponent<CameraFollow>().StartShake(1.5f, 0.16f);
		}
		if (floor % 10 == 0){
			if(fallSpeed > 0.5f) {
				GameObject.Find("Main Camera").GetComponent<CameraFollow>().StartShake(1.5f, 0.16f);
				fallSpeed -= 0.5f;
				SetMessage("The Tower has become more unstable!");
			} else {
				fallSpeed = 0.5f;
				GameObject.Find("Main Camera").GetComponent<CameraFollow>().StartShake(1.5f, 0.16f);
				SetMessage("The Tower is very unstable!");
			}
		}
		NewFloorName();
	}

	public string GetFloorName() {
		return currentFloorName;
	}

	public Sprite GetNextSectionSprite(int section) {
		Sprite result;
		if (sectionCount > 3) {
			sectionCount = 0;
			if(!isLastFloor()) {
				NewFloor();
			}
		}
		if(reverseFloorSprites) {
			result = currentFloor.sprites[(3 - section)];
		} else {
			result = currentFloor.sprites[section];
		}
		sectionCount++;
		return result;
	}

	private void SetNextStageColor() {
		float randGray = Random.Range(0.7f,1f);
		color = new Color(randGray, randGray, randGray);
	}

	public Color GetStageColor() {
		return color;
	}

	public GameObject GetFurniture() {
		return furniture[Random.Range(0,furniture.Length)];
	}

	public bool isLastFloor() {
		if(sectionCount == 4)
			return floor == lastFloor;
		else
			return false;
	}

	public void SpawnPuppy() {
		if(!puppySpawned && Random.Range(0,(3-sectionCount)) == 0) {
			puppySpawned = true;
			GameObject puppy = (GameObject)Instantiate(puppyPrefab, transform.position + new Vector3(Random.Range(-1.5f, 1.5f),2.5f,0), Quaternion.identity);
		}
	}

	private IEnumerator ResetLevel() {
		yield return new WaitForSeconds(3f);
		Application.LoadLevel(0);
	}

	public void GameOver() {
		gameOver = true;
		Debug.Log("Game Over");
		SetMessage("Game Over");
		StartCoroutine(ResetLevel());
	}

	public void Win() {
		win = true;
		SetMessage("You have saved your puppy!  You win!");
		Destroy(GameObject.Find("Earthquake"));
	}

	public void SetMessage(string s) {
		StartCoroutine(SetMessageText(s));
	}

	IEnumerator SetMessageText(string s) {
		message.text = messageOutline.text = s;
		yield return new WaitForSeconds(3f);
		message.text = messageOutline.text = "";
	}
}
