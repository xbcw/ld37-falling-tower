using UnityEngine;
using System.Collections;

public static class ExtensionMethods {

	// Takes a Vector3 and returns a Vector2
	public static Vector2 toVector2(this Vector3 v3) {
		return new Vector2(v3.x, v3.y);
	}
}