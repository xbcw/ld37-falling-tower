using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

	public LayerMask enemyMask;
	public float speed = 1;

	public bool isGrounded = false;
	public bool isBlocked = false;
	Transform tagGround;
	Rigidbody2D rb;
	float width;

	void Start () {
		rb = GetComponent<Rigidbody2D>();
		width = GetComponent<SpriteRenderer>().bounds.extents.x;

	}
	
	void FixedUpdate () {
		Vector2 lineCastPos = transform.position - transform.right * width;
		isGrounded = Physics2D.Linecast(lineCastPos, lineCastPos + Vector2.down * 0.1f, enemyMask);
		isBlocked = Physics2D.Linecast(lineCastPos, lineCastPos - transform.right.toVector2() * 0.05f, enemyMask);
		Debug.DrawLine(lineCastPos, lineCastPos + Vector2.down * 0.1f);
		Debug.DrawLine(lineCastPos, lineCastPos - transform.right.toVector2() * 0.05f);

		if(!isGrounded || isBlocked) {
			transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y + 180, transform.eulerAngles.z); 
		}

		rb.velocity = new Vector2(-transform.right.x * speed, rb.velocity.y);
	}
}
