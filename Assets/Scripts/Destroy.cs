﻿using UnityEngine;
using System.Collections;

public class Destroy : MonoBehaviour {

	public float delay = 2f;
	private float destroyTime;

	void Start() {
		destroyTime = Time.time + delay;
	}
	
	// Update is called once per frame
	void Update () {
		if(Time.time > destroyTime) {
			Destroy(gameObject);
		}
	}
}
