﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Furniture : MonoBehaviour {

    public float damageImmunityTime = 0.25f;
    public List<Color> colors;
    private float immuneTime;

    public GameObject pixelPrefab;

    void Start () {
        immuneTime = Time.time + damageImmunityTime;
    }

    // Update is called once per frame
    void OnCollisionEnter2D(Collision2D coll) {
        if(coll.gameObject.layer == LayerMask.NameToLayer("StaticFloor") || coll.gameObject.layer == LayerMask.NameToLayer("ActiveActor")) {
            gameObject.layer = LayerMask.NameToLayer("ActiveActor");
        }
    }

    void OnTriggerEnter2D(Collider2D coll) {
        if ((coll.gameObject.tag == "Ouch" && gameObject.layer == LayerMask.NameToLayer("ActiveActor")) || coll.gameObject.tag == "LastFloor") {
            PixelSpray(50);
            Destroy(gameObject);
        }
    }

    void PixelSpray(int numPixels) {
        for(int i=0;i<numPixels;i++) {
            GameObject pixel = (GameObject)Instantiate(pixelPrefab, transform.position, transform.rotation);
            pixel.GetComponent<SpriteRenderer>().color = colors[Random.Range(0, colors.Count)];
            Vector3 randomDirection = new Vector3(Random.Range(-1f,1f), Random.Range(0,1f), 0f);
            pixel.GetComponent<Rigidbody2D>().velocity = randomDirection * Random.Range(1f,3f) * 0.7f;
            pixel.transform.rotation = new Quaternion(Random.value, Random.value, 0f, 0f);
            pixel.transform.SetParent(GameObject.Find("PixelSpray").transform);
            pixel.transform.localScale = new Vector3(pixel.transform.localScale.x * Random.Range(0.8f, 1.2f), pixel.transform.localScale.y * Random.Range(0.8f, 1.2f), pixel.transform.localScale.z);
        }
    }
}