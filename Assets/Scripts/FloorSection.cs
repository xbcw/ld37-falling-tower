﻿using UnityEngine;
using System.Collections;

public class FloorSection : MonoBehaviour {

	public float shakeSpeed = 1f;
	public float shakeAmount = 1f;
	public float preFallTime = 1f;
	public int sectionNumber = 0;

	public GameObject newSectionPrefab;
	public GameObject particleSystemPrefab;

	private bool fallen = false;
	private GameObject ps;

	void Update() {
		if(transform.position.y < -3) {
			Destroy(gameObject);
		}
	}

	void Fall() {
		gameObject.layer = LayerMask.NameToLayer("Falling");
		gameObject.transform.Find("StagingFloorBarrier").gameObject.layer = LayerMask.NameToLayer("Falling");
		GameObject.Find("Main Camera").GetComponent<CameraFollow>().StartShake(0.15f, 0.03f);
		GameManager.instance.PlaySoundEffect(Random.Range(1,5),0.2f);
		if(!GameManager.instance.isLastFloor()) {
			SpawnSection();
		}
	}

	void SpawnSection() {
		GameObject newSection = (GameObject)Instantiate(newSectionPrefab);
		newSection.transform.SetParent(GameObject.Find("Stage").transform);
		newSection.transform.position = transform.position + new Vector3(0,2.6f,0);
		newSection.layer = LayerMask.NameToLayer("Floor");
		newSection.transform.Find("StagingFloorBarrier").gameObject.layer = LayerMask.NameToLayer("Ceiling");
		newSection.name = "Section";
		newSection.transform.Find("Background").GetComponent<SpriteRenderer>().color = GameManager.instance.GetStageColor();
		newSection.transform.Find("FloorSprite").gameObject.GetComponent<SpriteRenderer>().enabled = true;
		newSection.transform.Find("FloorSprite").gameObject.GetComponent<BoxCollider2D>().enabled = true;
		newSection.transform.Find("FloorSprite").gameObject.tag = "Ouch";
		newSection.transform.Find("Background").gameObject.GetComponent<SpriteRenderer>().sprite = GameManager.instance.GetNextSectionSprite(sectionNumber);
		if(GameManager.instance.reverseFloorSprites) {
			newSection.transform.Find("Background").gameObject.GetComponent<SpriteRenderer>().flipX = true;
		}
		if(GameManager.instance.floor == GameManager.instance.lastFloor) {
			newSection.tag = "LastFloor";
			GameManager.instance.SpawnPuppy();
		} else {
			SpawnFurniture(newSection.transform);
		}
	}

	void SpawnFurniture(Transform t) {
		GameObject furniture = (GameObject)Instantiate(GameManager.instance.GetFurniture(), transform.position + new Vector3(Random.Range(-1.5f, 1.5f),2.5f,0), Quaternion.identity);
	}

	void Shake() {
		//transform.Find("FloorSprite").rotation = new Quaternion(transform.rotation.x, transform.rotation.y, Mathf.Sin(Time.time * shakeSpeed * shakeAmount), 0f);
		//transform.Find("FloorSprite").position = transform.Find("FloorSprite").position + (Vector3)Random.insideUnitCircle * 0.1f;
		//transform.Find("FloorSprite").gameObject.GetComponent<Shake>().enabled = true;
		
	}

	void SpawnParticles() {
		if(!ps) {
			ps = (GameObject)Instantiate(particleSystemPrefab, transform.position + new Vector3(0,0.79f,0), Quaternion.identity);
			ps.transform.SetParent(transform);
			ps.transform.eulerAngles = new Vector3(90f,0f,0f);
			ps.transform.localScale = Vector3.one;
			ps.AddComponent<Destroy>();
			GameManager.instance.PlaySoundEffect(Random.Range(6,8),0f);
		}
	}

	void OnCollisionStay2D(Collision2D coll) {
		float rand = Random.Range(0.8f, 1.2f);
		//rand = 1;
		float fallTime = GameManager.instance.GetFallSpeed() * rand;
		if(gameObject.tag == "LastFloor" && !fallen && coll.gameObject.layer == LayerMask.NameToLayer("Ground")) {
			fallen = true;
			transform.Find("Background").position =  transform.Find("Background").position - new Vector3(0f,0.20f,0f);
			GameManager.instance.lastFloorCount++;
			if(GameManager.instance.lastFloorCount == 4) {
				GameManager.instance.Win();
			}
		}
		if(gameObject.tag != "LastFloor" && !fallen && coll.gameObject.layer == LayerMask.NameToLayer("Ground")) {
			fallen = true;
			transform.Find("FloorSprite").gameObject.GetComponent<SpriteRenderer>().enabled = false;
			transform.Find("FloorSprite").gameObject.GetComponent<BoxCollider2D>().enabled = false;
			Invoke("Fall", fallTime);
			Invoke("SpawnParticles",fallTime - 1.5f);
		}
		if(coll.gameObject.layer == LayerMask.NameToLayer("Ceiling")) {
			Invoke("SpawnParticles",fallTime - 1.5f);
		}
	}
}
