﻿using UnityEngine;
using System.Collections;

public class Shake : MonoBehaviour {

    public float shakeSpeed = 0.0f;
    public float shakeAmount = 0.0f;
    public float verticleShakeSpeed = 0.0f;
    public float verticleShakeAmount = 0.0f;
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector2(transform.position.x + Mathf.Sin(Time.time * shakeSpeed) * shakeAmount, transform.position.y + Mathf.Sin(Time.time * verticleShakeSpeed) * verticleShakeAmount);	
	}
}
