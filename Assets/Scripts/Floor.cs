using UnityEngine;
using System.Collections;

[System.Serializable]
public class Floor
{
    public string baseType;
    public string[] roomNames;
    public Sprite[] sprites;
    public bool useAdjective;
    
}