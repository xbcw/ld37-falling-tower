﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	public Transform target;
	public float offsetY = 0f;
	public float yFloor = 0f;
	public float UIHeight = 0f;
	private float offsetX = 0f;

	public float duration = 10f;
	public float magnitude = 1f;

	private float shakeX = 0f;
	private float shakeY = 0f;

	void Start () {
		yFloor -= UIHeight;
	}

	// Update is called once per frame
	void Update () {
		if(!GameManager.instance.gameOver) {
			float yPos = target.position.y + offsetY - UIHeight;
			if (yPos < yFloor) {
				yPos = yFloor;
			}
			float xPos = target.position.x + offsetX;
			transform.position = new Vector3(xPos + shakeX, yPos + shakeY, -10);
		}
	}

	public void StartShake(float d, float m) {
		if(d > duration) {
			duration = d;
		}
		if(m > magnitude) {
			magnitude = m;
		}
		StartCoroutine(Shake());
	}

	IEnumerator Shake() {

		float elapsed = 0.0f;

		Vector3 originalCamPos = Camera.main.transform.position;

		while (elapsed < duration) {

			elapsed += Time.deltaTime;          

			float percentComplete = elapsed / duration;         
			//float damper = 1.0f - Mathf.Clamp(4.0f * percentComplete - 3.0f, 0.0f, 1.0f);

	        // map value to [-1, 1]
			float x = Random.value * 2.0f - 1.0f;
			float y = Random.value * 2.0f - 1.0f;
			//x *= magnitude * damper;
			//y *= magnitude * damper;
			shakeX = magnitude * Mathf.PerlinNoise(x,y) * Random.Range(-1f,1f);
			shakeY = magnitude * Mathf.PerlinNoise(x,y) * Random.Range(-1f,1f);

			//Camera.main.transform.position = new Vector3(originalCamPos.x + x, originalCamPos.y + y, originalCamPos.z);

			yield return null;
		}
		shakeX = shakeY = 0f;
		duration = 0f;
		magnitude = 0f;
		//Camera.main.transform.position = originalCamPos;
	}
}
