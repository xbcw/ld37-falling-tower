﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Actor : MonoBehaviour {

	public float maxHealth = 1f;
	private float health;

	public Image lifeBarOutline;
	public Image lifeBar;

	public GameObject pixelPrefab;
	public List<Color> colors;

	void Start() {
		health = maxHealth;
		lifeBarOutline.fillAmount = maxHealth;
		lifeBar.fillAmount = health;
	}

	void Update() {
		lifeBar.fillAmount = health;
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if (!GameManager.instance.win && coll.gameObject.tag == "Ouch") {
			health -= GameManager.instance.GetSectionDamage();
			if(health <= 0) {
				PixelSpray(100);
				GameManager.instance.GameOver();
				Destroy(gameObject);
			}
		}
	}

    void PixelSpray(int numPixels) {
        for(int i=0;i<numPixels;i++) {
            GameObject pixel = (GameObject)Instantiate(pixelPrefab, transform.position, transform.rotation);
            pixel.GetComponent<SpriteRenderer>().color = colors[Random.Range(0, colors.Count)];
            Vector3 randomDirection = new Vector3(Random.Range(-1f,1f), Random.Range(0,1f), 0f);
            pixel.GetComponent<Rigidbody2D>().velocity = randomDirection * Random.Range(1f,3f) * 0.7f;
            pixel.transform.rotation = new Quaternion(Random.value, Random.value, 0f, 0f);
            pixel.transform.SetParent(GameObject.Find("PixelSpray").transform);
            pixel.transform.localScale = new Vector3(pixel.transform.localScale.x * Random.Range(0.8f, 1.2f), pixel.transform.localScale.y * Random.Range(0.8f, 1.2f), pixel.transform.localScale.z);
        }
    }
}